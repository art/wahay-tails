#!/usr/bin/env bash
set -x

echo "adding the signal GPG key to APT keyring"
curl https://dl.autonomia.digital/debian/keys.asc -o keys.asc
sudo apt-key add keys.asc
sudo apt-key list debian@autonomia.digital


echo "Add the signal repo to APT"
echo 'deb tor+https://dl.autonomia.digital/debian buster main deb-src tor+https://dl.autonomia.digital/debian buster main' | sudo tee -a /etc/apt/sources.list

echo "Updating packages list"
apt update

echo "Downloading Wahay"
sudo apt-get -y install wahay

rm keys.asc


