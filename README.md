# wahay-tails


* First start Tails with Administrator password:

![gif tails root](https://wahay.org/assets/img/documentation/get-started/installation/tails-root-password.gif)


* Open the Terminal application
* Copy and paste in the terminal following command

```wget https://0xacab.org/art/wahay-tails/-/raw/no-masters/install/install.sh -O install-wahay.sh && sudo bash install-wahay.sh```

